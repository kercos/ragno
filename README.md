# RAGNO
Code for the Telegram Data Clustering Contest Round 2 \
https://contest.com/docs/data_clustering2

Authors:
- Angelo Basile me@angelobasile.it http://anbasile.github.io/
- Federico Sangati federico.sangati@gmail.com https://fede.sangati.me

Repository:
- https://gitlab.com/kercos/ragno

Installation procedure in clean debian 10.1:

- `apt-get update`
- `xargs -a deb-packages.txt apt-get install -y`
- `python3 -m venv env`
- `source ./env/bin/activate`
- `pip install -r ./requirements.txt`


Usage:
- `./tgnews languages <source_dir> [<output_dir>]`
- `./tgnews news <source_dir> [<output_dir>]`
- `./tgnews categories <source_dir> [<output_dir>]`
- `./tgnews threads <source_dir> [<output_dir>]`
- `./tgnews server <port_number>`

Test:
- `php dc-check/dc-check.php tgnews languages <source_dir>`
- `php dc-check/dc-check.php tgnews news <source_dir>`
- `php dc-check/dc-check.php tgnews categories <source_dir>`
- `php dc-check/dc-check.php tgnews threads <source_dir>`
- `php dc-check/dc-check.php tgnews server <port_number> <source_dir>`
- `php dc-check/dc-check.php tgnews all <port_number> <source_dir>`

Dir Structue:
- README: this file
- tgnews: binary executable
- deb-packages.txt: debian packages
- src: code
- res: resources
