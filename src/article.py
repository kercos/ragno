from dataclasses import dataclass
from datetime import datetime
from functools import total_ordering

@dataclass(init=True, repr=True, eq=False, order=False, unsafe_hash=False, frozen=True)
@total_ordering
class Article:
    name: str
    published: datetime
    expiration: datetime
    meta_info: dict = None
    body_text: str = ''

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.expiration == other.expiration and self.name == other.name
        else:
            return False

    def __lt__(self, other):
        if self.expiration < other.expiration:
            return True
        if self.expiration > other.expiration:
            return False            
        return self.name < other.name

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return f'{self.name} pub={self.published} exp={self.expiration}'

def from_timetamps_str(name, published, expiration):
    from datetime import datetime
    return Article(
        name = name,
        published = datetime.fromisoformat(published),
        expiration = datetime.fromisoformat(expiration)
    )