from src.article import Article
from src.article_set import ArticleSet
from src.common import LANGUAGES, CATEGORIES
from src.server import log
from datetime import datetime, timedelta   
from src import utils
from src import task1, task2, task3, task4
import pickle

class ArticleIndex():
    
    def __init__(self):

        self.ART_NAME_INFO = {} # art_name -> {'path': <keys-path>, 'expiration': <exp>}

        self.ART_SORTED_STRUCT = {
            'RANKING': {lang:{cat: ArticleSet() for cat in CATEGORIES} for lang in LANGUAGES},
            'OTHERS': ArticleSet()
        }

        lang_pipelines = {}
        for lang in LANGUAGES:
            with open('./res/task3/task3_model_{}_good.pickle'.format(lang), 'rb') as f:
                lang_pipelines[lang]= pickle.load(f)

        self.CAT_CLASSIFIER = task3.ClassifyClass(
            cat_articles = None, 
            lang_pipelines = lang_pipelines, 
            output_dir = None
        )

        self.LAST_PUBLISHED_DATE = datetime.fromisoformat('1900-01-01')

    def get_article_set(self, index_key_path):
        o = self.ART_SORTED_STRUCT
        for key in index_key_path:
            o = o[key]
        return o

    def add_or_update(self, article_name, html_content, ttl_sec):
        meta_info, body_text = utils.parse_html(html_content, is_file=False)

        published = datetime.fromisoformat(meta_info['date_time']).replace(tzinfo=None)
        expiration = published + timedelta(seconds=ttl_sec)

        # removed expired articles in indexed strucure
        self.remove_all_expired_articles(published)
        
        article = Article(
            name = article_name,
            published = published,
            expiration = expiration,
            meta_info = meta_info,
            body_text = body_text
        )

        art_info = self.ART_NAME_INFO.get(article_name, None)
        if art_info:
            s = self.get_article_set(art_info['path'])
            old_art = Article(
                name=article_name, 
                published=art_info['published'], 
                expiration=art_info['expiration']
            )
            s.remove(old_art)
            s.add(article)
            log.info('Updated article in {}:'.format(art_info['path']))
            log.info('\tOld article: {}'.format(old_art))
            log.info('\tNew article: {}'.format(article))
            return False
        else:
            is_news, _ = task2.decide_if_news_based_on_url(body_text, meta_info)
            index_key_path = ('OTHERS',)
            if is_news:
                lang, _ = task1.infer_lang(meta_info, body_text)
                if lang in LANGUAGES:
                    cat = self.CAT_CLASSIFIER.classify_simple(lang, meta_info, body_text)
                    index_key_path = ('RANKING', lang, cat)
                                    
            self.ART_NAME_INFO[article_name] = {
                'path': index_key_path, 
                'published': published, 
                'expiration': expiration
            }
            s = self.get_article_set(index_key_path)
            s.add(article)
            log.info('Added article {} in {}'.format(article, index_key_path))
            return True

    def remove_article(self, article_name):
        art_info = self.ART_NAME_INFO.get(article_name, None)
        if art_info:
            s = self.get_article_set(art_info['path'])
            article = Article(
                name=article_name, 
                published=art_info['published'], 
                expiration=art_info['expiration']
            )
            log.info('Deleting {}'.format(article))
            s.remove(article)
            del self.ART_NAME_INFO[article_name]
            return True
        return False

    def remove_all_expired_articles(self, timestamp):
        # If more than TTL seconds passed between the publishing date of the current 
        # article and the latest article in the index, the current article should 
        # be removed from the index.

        if timestamp <= self.LAST_PUBLISHED_DATE:
            return

        log.info('Removing articles expired before {}'.format(timestamp))

        for lang in LANGUAGES:
            for cat in CATEGORIES:
                deleted_arts = self.ART_SORTED_STRUCT['RANKING'][lang][cat].remove_expired(timestamp)
                for art in deleted_arts: # remove from path index
                    del self.ART_NAME_INFO[art.name]

        deleted_arts = self.ART_SORTED_STRUCT['OTHERS'].remove_expired(timestamp)
        for art in deleted_arts: # remove from path index
            del self.ART_NAME_INFO[art.name]

        self.LAST_PUBLISHED_DATE = timestamp

    def thread_ranking(self, period_sec, lang, cat):
        now = datetime.utcnow()
        from_timestamp = now - timedelta(seconds=period_sec)
        if cat in CATEGORIES:
            s = self.ART_SORTED_STRUCT['RANKING'][lang][cat]
            articles = s.get_article_published_from_timestamp(from_timestamp)
            processed_articles = task4.prepare_articles(articles, lang)
            threads, _ = task4.run_clustering(processed_articles, output_dir=None)
        else:
            assert cat=='any'
            threads = []
            for cat in CATEGORIES:
                s = self.ART_SORTED_STRUCT['RANKING'][lang][cat]                
                articles = s.get_article_published_from_timestamp(from_timestamp)
                processed_articles = task4.prepare_articles(articles, lang)
                cat_threads, _ = task4.run_clustering(processed_articles, output_dir=None)
                for t in cat_threads:
                    t['category'] = cat
                threads.extend(cat_threads)
        result = {
            "threads": threads
        }
        return result
        
    def debug_set(self, article_name):
        art_info = self.ART_NAME_INFO.get(article_name, None)
        s = self.get_article_set(art_info['path'])
        name = article_name[:-5]
        with open('./debug-{}.log'.format(name), 'w') as f_out:
            for art in s:
                f_out.write(str(art) + '\n')


if __name__ == '__main__':
    from datetime import datetime
    AI = ArticleIndex()
    from src.article import from_timetamps_str
    s = ArticleSet()
    a1 = from_timetamps_str('1001112537429184539.html', '2020-05-04 00:10:00', '2020-05-04 00:15:00') 
    a2 = from_timetamps_str('1006407625940184388.html', '2020-05-04 00:10:00', '2020-05-04 00:05:00') 
    a3 = from_timetamps_str('1003461416076475711.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a4 = from_timetamps_str('1006407625940184388.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a5 = from_timetamps_str('1010998403498845489.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a6 = from_timetamps_str('1010998403802709347.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a7 = from_timetamps_str('1010998403963677263.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a8 = from_timetamps_str('1010998404456081585.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a9 = from_timetamps_str('1010998404737826944.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a10 = from_timetamps_str('1010998404855370901.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a11 = from_timetamps_str('1010998404892651830.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a12 = from_timetamps_str('1010998405037008329.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a13 = from_timetamps_str('1010998405210454562.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a14 = from_timetamps_str('1010998405453177267.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a15 = from_timetamps_str('1010998405461520688.html', '2020-05-04 00:00:00', '2020-05-04 00:05:00')
    # a16 = from_timetamps_str('1030633937792299482.html', '2020-05-04 00:14:06', '2020-05-04 00:19:06')
    s.add(a1)
    s.add(a2)
    s.add(a3)
    # s.add(a4)
    # s.add(a5)
    # s.add(a6)
    # s.add(a7)
    # s.add(a8)
    # s.add(a9)
    # s.add(a10)
    # s.add(a11)
    # s.add(a12)
    # s.add(a13)
    # s.add(a14)
    # s.add(a15)
    # s.add(a16)
    s.remove_expired(datetime.fromisoformat('2020-05-04 00:19:15'))