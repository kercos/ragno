from sortedcontainers import SortedSet
from src.server import log

class ArticleSet(SortedSet):
    
    def remove_expired(self, timestamp):
        deleted = []
        for art in self:
            if art.expiration < timestamp:
                log.info('Deleting {}'.format(art))
                deleted.append(art)                
            else:
                # following articles can't be expired
                break
        self.difference_update(deleted)
        # log.info("Removed {} articles".format(len(to_delete)))
        return deleted

    def get_article_published_from_timestamp(self, timestamp):
        return [art for art in self if art.published >= timestamp]
