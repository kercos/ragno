import multiprocessing
import random

random.seed(0)

NUM_CORES = multiprocessing.cpu_count()

LANGUAGES = [
    'en',  # English
    'ru',  # Russian
]

CATEGORIES = [
    "society", 
    "economy", 
    "technology", 
    "sports", 
    "entertainment", 
    "science", 
    "other"
]


