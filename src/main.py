 #!/usr/bin/env python3

import sys
import os   
from src import task1, task2, task3, task4, task5, server
import time

def run():

    start_sec = time.time()

    # checking number of arguments
    args_number = len(sys.argv)
    if args_number not in [3,4]:
        print("Wrong number of arguments (should be 3 instead of {})".format(args_number))
        sys.exit(1)

    # parsing task
    task = sys.argv[1]

    if task == 'server':
        port = sys.argv[2]
        server.server_start(port)
        return

    # mapping tasks to functions
    tasks_functions = {
        'languages': task1.run_languages_task,
        'news': task2.run_news_task,
        'categories': task3.run_categories_task,
        'threads': task4.run_threads_task,
        # 'top': task5.run_top_task
    }
    if task not in tasks_functions:
        print("Specified task ({}) is not a valid task".format(task))
        sys.exit(1)  

    # parsing the source_dir
    source_dir = sys.argv[2]
    if not os.path.isdir(source_dir):
        print("Specified source_dir ({}) does not exist or is not a directory".format(source_dir))
        sys.exit(1)    

    # parsing output_dir (optional)
    output_dir = sys.argv[3] if args_number==4 else None
    print_to_stdout = not output_dir

    # execute function for that task
    tasks_functions[task](source_dir, output_dir, print_to_stdout)

    if output_dir:
        from src import utils
        total_files = utils.get_total_html_files_in_dir(source_dir)
        took_sec = (time.time() - start_sec)
        sec_per_thousand = took_sec/total_files*1000        
        print("Took {} sec ({} sec per 1000 files)".format(int(took_sec),int(sec_per_thousand)))


if __name__ == "__main__":    
    run()
    

