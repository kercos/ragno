from flask import Flask, request, jsonify
import logging
from src.common import LANGUAGES, CATEGORIES

app = Flask(__name__)
app.logger.disabled = True
log = logging.getLogger('werkzeug')
log.disabled = True    

INITIALIZED = False

ARTICLES_INDEX = None

def initialize():    
    global INITIALIZED
    global ARTICLES_INDEX

    from src.article_index import ArticleIndex
    ARTICLES_INDEX = ArticleIndex()
        
    INITIALIZED = True



@app.route('/<article_name>', methods=['PUT'])
def put_article(article_name=None):    
    
    log.info('API PUT REQUEST: {}'.format(article_name))

    if not INITIALIZED:
        return ('Service Unavailable', 503)
    
    headers = request.headers
    ttl_sec = int(headers['Cache-Control'].split('=')[1])
    
    html_content = request.data.decode('utf-8') 

    if ARTICLES_INDEX.add_or_update(article_name, html_content, ttl_sec):
        return 'created', 201
    return '', 204
    

@app.route('/<article_name>', methods=['DELETE'])
def delete_article(article_name=None):    

    log.info('API DELETE REQUEST: {}'.format(article_name))
    
    if not INITIALIZED:
        return ('Service Unavailable', 503)    

    if ARTICLES_INDEX.remove_article(article_name):
        return '', 204
    return 'Not found', 404


@app.route('/threads', methods=['GET'])
def get_ranking(article_name=None):    
    
    if not INITIALIZED:
        return ('Service Unavailable', 503)    

    period_sec = request.args.get('period')
    lang = request.args.get('lang_code')
    cat = request.args.get('category') # could by 'any'

    try:
        period_sec = int(period_sec)
    except ValueError:
        return 'period should be an int', 400

    if lang not in LANGUAGES:
        return 'lang_code should be en or ru', 400

    if cat not in CATEGORIES and cat!='any':
        return 'invalid category', 400

    result = ARTICLES_INDEX.thread_ranking(period_sec, lang, cat)

    return jsonify(result), 200


    
def server_start(port):
    import threading
    threading.Thread(target=app.run, kwargs={'port':port, 'threaded':False}).start()
    initialize()

if __name__ == '__main__':
    server_start(8000)
    