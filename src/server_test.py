import os
import requests
import random

random.seed(111)

dir = 'data2/DataClusteringSample0410/20200504/00/'
files = sorted(os.listdir(dir))

def put_article(f_name):
    with open(os.path.join(dir, f_name)) as f_in:
        content = f_in.read().encode('utf-8')
        url = "http://localhost:8000/" + f_name
        ttl_sec = random.randint(300, 2592000)
        headers = {
            'Cache-Control': 'max-age={}'.format(ttl_sec),
            'Content-Type': 'text/html'
        }
        response = requests.request("PUT", url, headers=headers, data = content)
        if response.status_code == 500:
            raise ValueError('Status code 500')

def delete_article(f_name):            
    url = "http://localhost:8000/" + f_name    
    response = requests.request("DELETE", url)
    if response.status_code == 500:
        raise ValueError('Status code 500')


def put_all_article():    
    for f_name in files:        
        if not f_name.endswith('.html'):
            continue
        put_article(f_name)

def put_random_article():    
    f_name = random.choice(files)     
    if not f_name.endswith('.html'):
        put_random_article()
    put_article(f_name)

def delete_all_article():    
    for f_name in files:        
        if not f_name.endswith('.html'):
            continue
        delete_article(f_name)

def delete_random_article():    
    f_name = random.choice(files)     
    if not f_name.endswith('.html'):
        delete_random_article()
    delete_article(f_name)

if __name__ == '__main__':
    # put_all_article()
    # delete_all_article()
    pass