from src import common

def read_freq_domain_file(input_file):
    domain_freq = {}
    with open(input_file) as f_in:
        for l in f_in:
            domain_freq[l[1]]=[int(l[0])]

LANG_FREQ_DOMAINS = {
    lang: read_freq_domain_file('res/freq_domains/{}_domains.txt'.format(lang))
    for lang in common.LANGUAGES
}

def get_importance(lang, domain):
    return LANG_FREQ_DOMAINS.get(domain, 1)

def get_importance_multi(lang, domain_list):
    imp = sum(get_importance(lang, d) for d in domain_list)
    return imp/len(domain_list) # average

if __name__ == '__main__':
    pass