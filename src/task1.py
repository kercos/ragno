import json
import os
from shutil import copyfile
from multiprocessing import Pool, Manager
from langid.langid import LanguageIdentifier, model
from src import utils
from src import common

'''
Launch parameters:

tgnews languages source_dir
The result must be output to STDOUT in JSON format.

Output format:

[
  {
    "lang_code": "en",
    "articles": [
      "981787246124324.html",
      "239748235923753.html",
      ...
    ]
  },
  {
    "lang_code": "ru",
    "articles": [
      "273612748127432.html",
      ...
    ]
  },
  ...
]
where:

lang_code – ISO 639-1 two-letter language code
articles – list of file names containing texts in lang_code language
'''


'''
Init lang identifier
'''
identifier = LanguageIdentifier.from_modelstring(model, norm_probs=True)


def infer_lang(meta_info, body_text):
    if meta_info['site_name'].endswith('.ru'):
        lang, prob = 'ru', 1.
    else:    
        lang, prob = identifier.classify(body_text)

    if lang not in common.LANGUAGES:
        return None, None

    return lang, prob

class ClassifyClass:

    def __init__(self, lang_results, output_dir):
        self.lang_results = lang_results
        self.output_dir = output_dir

    def classify(self, f):
        file_basename = os.path.basename(f)     

        meta_info, body_text = utils.parse_html(f)        
        lang, prob = infer_lang(meta_info, body_text)

        if lang is None:
            return

        self.lang_results[lang].append(file_basename)

        if self.output_dir:                        
            prob_percent = int(prob*100)
            for file_format in ['txt','html']:
                file_name = file_basename if file_format=='html' else file_basename.replace('.html', '.txt')
                output_file_path = os.path.join(self.output_dir, file_format, lang, str(prob_percent), file_name)
                parent_dir = os.path.dirname(output_file_path)
                if not os.path.exists(parent_dir):
                    os.makedirs(parent_dir)
                if file_format == 'html':                    
                    copyfile(f, output_file_path) # copy file
                else: # txt               
                    with open(output_file_path, 'w') as f_out:
                        meta_info_txt = json.dumps(meta_info, ensure_ascii=False, indent=3)
                        meta_info_body_txt = '\n\n'.join([meta_info_txt, body_text])     
                        f_out.write(meta_info_body_txt)

'''
Run as python main.py languages source_dir [output_dir]
'''
def run_languages_task(source_dir, output_dir=None, print_to_stdout=True):

    # do not print progress bar if printing result to stdout
    print_progress = not print_to_stdout 

    # generator of html files in source dir
    html_files_generator = utils.generate_all_html_files_in_dir(source_dir, print_progress)

    with Manager() as manager:

        # init result in a manager var (multiprocessing safe)
        lang_results = manager.dict({lang:manager.list() for lang in common.LANGUAGES})        

        classify_fun = ClassifyClass(lang_results,output_dir).classify

        with Pool(common.NUM_CORES) as p:
            p.map(classify_fun, html_files_generator)
        
        result_formatted = [
            {
                "lang_code": lang,
                "articles": sorted(articles)
            } for lang, articles in lang_results.items()
        ]

    if output_dir:
        json_file = os.path.join(output_dir, 'results.json')
        utils.print_to_json(result_formatted, json_file)
    if print_to_stdout:        
        result_en_ru_str = json.dumps(result_formatted, ensure_ascii=False, indent=3)
        print(result_en_ru_str)

if __name__ == "__main__":
    pass
