import json
import os
from shutil import copyfile
from multiprocessing import Pool, Manager
from src import utils
from src import common

'''
Launch parameters:

tgnews news source_dir
The result must be output to STDOUT in JSON format.

Output format:

{
  "articles": [
    "981787246124324.html",
    ...
  ]
}
where:

articles – list of file names containing news
'''


OUTPUT_CLASS_SIZE = 50 # SIZE OF CLASS (0-50, 50-100, 100-150, ...) only used when files are in output
TEXT_SIZE_THRESHOLD = 350


def decide_if_news_based_on_url(body_text, meta_info):
    """
        If the url of an article contains the string
        '/news/', then we classify the article as being
        a news. False otherwise.
    """
    text_size = len(body_text)    
    size_number = int(text_size/OUTPUT_CLASS_SIZE)
    size_class = '{}-{}'.format(
        size_number*OUTPUT_CLASS_SIZE, 
        (size_number+1)*OUTPUT_CLASS_SIZE)

    is_news = '/news/' in meta_info['url']
    return is_news, size_class

class ClassifyClass:
    
    def __init__(self, news_articles, output_dir):
        self.news_articles = news_articles
        self.output_dir = output_dir

    def classify(self, f):
        file_basename = os.path.basename(f)
        meta_info, body_text = utils.parse_html(f)
        is_news, size_class = decide_if_news_based_on_url(body_text, meta_info)
        if is_news:
            self.news_articles.append(file_basename)            
        if self.output_dir:
            for file_format in ['txt','html']:
                file_name = file_basename if file_format=='html' else file_basename.replace('.html', '.txt')
                news_other = 'news' if is_news else 'others'
                output_file_path = os.path.join(self.output_dir, file_format, news_other, str(size_class), file_name)
                parent_dir = os.path.dirname(output_file_path)
                if not os.path.exists(parent_dir):
                    os.makedirs(parent_dir)
                if file_format == 'html':                    
                    copyfile(f, output_file_path) # copy file
                else: # txt               
                    with open(output_file_path, 'w') as f_out:
                        meta_info_txt = json.dumps(meta_info, ensure_ascii=False, indent=3)
                        meta_info_body_txt = '\n\n'.join([meta_info_txt, body_text])     
                        f_out.write(meta_info_body_txt)


'''
Run as python main.py news source_dir [output_dir]
'''
def run_news_task(source_dir, output_dir=None, print_to_stdout=True):

    # do not print progress bar if printing result to stdout    
    print_progress = not print_to_stdout     

    # generator of html files in source dir
    html_files_generator = utils.generate_all_html_files_in_dir(source_dir, print_progress)

    with Manager() as manager:

        news_articles = manager.list()    

        classify_fun = ClassifyClass(news_articles,output_dir).classify

        with Pool(common.NUM_CORES) as p:
            p.map(classify_fun, html_files_generator)

        result = {
            "articles": sorted(news_articles)
        }

    if output_dir:
        json_file = os.path.join(output_dir, 'results.json')
        utils.print_to_json(result, json_file)
    if print_to_stdout:
        result_str = json.dumps(result, ensure_ascii=False, indent=3)
        print(result_str)


if __name__ == "__main__":
    pass
