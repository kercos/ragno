import os
import pickle
from shutil import copyfile
import pandas as pd
import re
import argparse
import json
from collections import defaultdict
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.svm import LinearSVC
from sklearn.metrics import classification_report as cfr
from multiprocessing import Pool, Manager
from src import utils as utils
from src import task1
from src import common

'''
Launch parameters:

tgnews categories source_dir
The result must be output to STDOUT in JSON format.

Output format:

[
  {
    "category": "society",
    "articles": [
      "981787246124324.html",
      ...
    ]
  },
  {
    "category": "sports",
    "articles": [
      "2348972396239813.html",
      ...
    ]
  },
  ...
]
where:

category – "society", "economy", "technology", "sports", "entertainment", "science" or "other"
articles – list of file names containing news articles that match the category
'''

RE_URL_PARTS = re.compile(r'[/.]')


with open('./res/task3/task3_keywords.json') as f_in:
    CATEGORIES_URL_KEYWORDS = json.load(f_in)

CATS = CATEGORIES_URL_KEYWORDS.keys()



def analyze_site_names(source_dir, output_dir, lang, output_html_files):
    domains_freq = defaultdict(int)
    base_urls = set()
    url_part_freq = defaultdict(int)

    if not os.path.exists(output_dir): 
        os.makedirs(output_dir)

    for f in utils.generate_all_html_files_in_dir(source_dir, print_progress=True):
        file_basename = os.path.basename(f)
        meta_info, _ = utils.parse_html(f)
        base_url, domain = utils.get_base_url_and_domain(meta_info)        
        url_parts = [p for p in RE_URL_PARTS.split(base_url) if p]
        base_urls.add(base_url)
        domains_freq[domain] += 1
        for p in url_parts: 
            url_part_freq[p] += 1        
        if output_html_files:
            output_file_path = os.path.join(output_dir, 'html', domain, file_basename)        
            parent_dir = os.path.dirname(output_file_path)
            if not os.path.exists(parent_dir): 
                os.makedirs(parent_dir)
            copyfile(f, output_file_path) # copy file

    url_part_freq = {p:f for p,f in url_part_freq.items() if f >= 5}

    sorted_domains_file_path = os.path.join(output_dir, '{}_domains.txt'.format(lang))
    utils.print_dictint_to_file_sorted(sorted_domains_file_path, domains_freq)
      

    sorted_base_urls_file_path = os.path.join(output_dir, '{}_base_urls.txt'.format(lang))
    utils.print_sorted_collection_to_file(base_urls, sorted_base_urls_file_path)

    sorted_urls_parts_file_path = os.path.join(output_dir, '{}_url_parts.txt'.format(lang))
    utils.print_dictint_to_file_sorted(sorted_urls_parts_file_path, url_part_freq)


def infer_category_from_base_url(base_url, lang):
    base_url_parts = [p for p in RE_URL_PARTS.split(base_url) if p] # split on dots and slashes
    categories = [
        k for k,v in CATEGORIES_URL_KEYWORDS.items() 
        if any(e in base_url_parts for e in v[lang])
    ]
    # discard cases where 0 or more than one cat is found in url        
    if len(categories)==1:         
        return categories[0]    
    return None


def create_cat_data_set(source_dir, output_dir, lang, output_html_files):
        
    cat_df_columns = ['category', 'url', 'site_name', 'title', \
        'date_time', 'description', 'author', 'body_text']
    cat_df = pd.DataFrame(columns=cat_df_columns)
    i = 0 # data record indexer
    cat_size = {k:0 for k in CATS}    
    cat_base_urls = {k:[] for k in CATS}    
    for f in utils.generate_all_html_files_in_dir(source_dir, print_progress=True):
        file_basename = os.path.basename(f)
        meta_info, body_text = utils.parse_html(f)
        base_url, _ = utils.get_base_url_and_domain(meta_info)        
        cat = infer_category_from_base_url(base_url, lang)
        if not cat:
            continue
        cat_base_urls[cat].append(base_url)
        if output_html_files:
            output_file_path = os.path.join(output_dir, 'html', cat, file_basename)
            parent_dir = os.path.dirname(output_file_path)
            if not os.path.exists(parent_dir):
                os.makedirs(parent_dir)        
            copyfile(f, output_file_path) # copy file
        all_info = meta_info.copy()
        all_info['category'] = cat
        all_info['body_text'] = body_text        
        cat_df.loc[i] = all_info     
        cat_size[cat] += 1   
        i += 1

    min_set_size = min(cat_size.values())
    max_set_size = max(cat_size.values())
    cat_df_grouped = cat_df.groupby('category', group_keys=False)
    sampled_df_cats = cat_df_grouped.apply(lambda x: x.sample(n=min_set_size, random_state=1))

    cat_sizes = ['{}:{}'.format(k,v) for k,v in cat_size.items()]
    print("Cat set sizes: {}".format(', '.join(cat_sizes)))    
    print("Min Set Size: {}".format(min_set_size))    
    print("Max Set Size: {}".format(max_set_size))    

    # min sampled (balanced)
    json_file_name = 'cat_dataset_{}_{}.jsonl'.format(lang, min_set_size)
    json_file_path = os.path.join(output_dir, json_file_name)
    parent_dir = os.path.dirname(json_file_path)
    if not os.path.exists(parent_dir):
        os.makedirs(parent_dir)        
    sampled_df_cats.to_json(json_file_path, orient='records', lines=True, force_ascii=False)

    # upsampled (balanced)
    up_sampled_df_cats = cat_df_grouped.apply(lambda x: x.sample(n=max_set_size, random_state=1, replace=True))
    json_file_name = 'cat_dataset_{}_{}_upsampled.jsonl'.format(lang, max_set_size)
    json_file_path = os.path.join(output_dir, json_file_name)
    up_sampled_df_cats.to_json(json_file_path, orient='records', lines=True, force_ascii=False)

    # all (unbalanced)
    json_file_name = 'cat_dataset_{}_all_unbalanced.jsonl'.format(lang)
    json_file_path = os.path.join(output_dir, json_file_name)
    cat_df.to_json(json_file_path, orient='records', lines=True, force_ascii=False)
    
    # print base_urls
    base_urls_dir = os.path.join(output_dir, 'base_urls')
    if not os.path.exists(base_urls_dir):
        os.makedirs(base_urls_dir)
    for c in CATS:
        output_file_path = os.path.join(base_urls_dir, '{}.txt'.format(c))
        with open(output_file_path, 'w') as f_out:
            f_out.write('\n'.join(sorted(cat_base_urls[c])))

class Train:
    def __init__(self):
        self.df = pd.read_json('./res/task3_dataset/cat_dataset_en_500.json', lines=True) # need to use the new one
        return None

    def run(self):
        df = self.df.sample(frac=1).reset_index(drop=True)
        train = df.iloc[:1500]
        test = df.iloc[1500:]
        pipeline = Pipeline([
            ('features', HashingVectorizer(ngram_range=(1,3))),
            ('cls', LinearSVC())])
        pipeline.fit(train.body_text, train.category)
        y_preds = pipeline.predict(test.body_text)
        print(cfr(test.category, y_preds))

        with open('./res/task3/task3_model.pickle', 'wb+') as f:
            pickle.dump(pipeline, f)

        return None


class Evaluate:
    def __init__(self, df):
        self.df = pd.read_json(df, lines=True)
        return None

    def run(self):
        with open('./res/task3/task3_model.pickle', 'rb') as f:
            pipeline = pickle.load(f)

        y_preds = pipeline.predict(self.df.body_text)
        print(cfr(self.df.category, y_preds))
        return None


def upsample(input_file_jsonl, output_file_jsonl, group_size):
    df = pd.read_json(input_file_jsonl, lines=True)
    grouped = df.groupby('category', group_keys=False)
    upsampled = grouped.apply(lambda x: x.sample(n=group_size, replace=True))
    upsampled.to_json(output_file_jsonl, orient='records', lines=True, force_ascii=False)


def run_sites_analysis(lang, output_html_files):
    analyze_site_names(
        './data/results/Task2/all/{}/html/news'.format(lang), 
        './data/results/Task3/Sites/{}'.format(lang), 
        lang, # en, ru
        output_html_files # bool
    )

def build_dataset(lang, output_html_files):
    create_cat_data_set(
        './data/results/Task2/all/{}/html'.format(lang), 
        './data/results/Task3/CatDataSet/all/{}/'.format(lang), 
        lang, # en, ru
        output_html_files
    )
    # English stats:
    # society:7254, 
    # economy:10507, 
    # technology:1909, 
    # sports:18665, 
    # entertainment:10360, 
    # science:2147, 
    # other:932 <- min
    # 
    # Russian stats:
    # society:28694, 
    # economy:4602, 
    # technology:2114, 
    # sports:8587, 
    # entertainment:10369, 
    # science:2581, 
    # other:281 <- min

class ClassifyClass:
    
    def __init__(self, cat_articles, lang_pipelines, output_dir):
        self.cat_articles = cat_articles
        self.lang_pipelines = lang_pipelines
        self.output_dir = output_dir

    def classify_simple(self, lang, meta_info, body_text):
        base_url, _ = utils.get_base_url_and_domain(meta_info)
        cat = infer_category_from_base_url(base_url, lang)
        predicted_cat = cat if cat else self.lang_pipelines[lang].predict([body_text])[0]
        return predicted_cat

    def classify(self, f):
        file_basename = os.path.basename(f)
        meta_info, body_text = utils.parse_html(f)
        base_url, _ = utils.get_base_url_and_domain(meta_info)
        lang, _ = task1.infer_lang(meta_info, body_text)

        if lang is None:
            return

        cat = infer_category_from_base_url(base_url, lang)
        predicted_cat = cat if cat else self.lang_pipelines[lang].predict([body_text])[0]
        self.cat_articles[predicted_cat].append(file_basename)
        if self.output_dir:
            for file_format in ['txt','html']:
                file_name = file_basename if file_format=='html' else file_basename.replace('.html', '.txt')
                output_file_path = os.path.join(self.output_dir, file_format, predicted_cat, file_name)
                parent_dir = os.path.dirname(output_file_path)
                if not os.path.exists(parent_dir):
                    os.makedirs(parent_dir)
                if file_format == 'html':                    
                    copyfile(f, output_file_path) # copy file
                else: # txt               
                    with open(output_file_path, 'w') as f_out:
                        meta_info_txt = json.dumps(meta_info, ensure_ascii=False, indent=3)
                        meta_info_body_txt = '\n\n'.join([meta_info_txt, body_text])     
                        f_out.write(meta_info_body_txt)


def run_categories_task(source_dir, output_dir=None, print_to_stdout=True, print_progress=None):

    # do not print progress bar if printing result to stdout    
    print_progress = print_progress if print_progress!=None else not print_to_stdout     

    # generator of html files in source dir
    html_files_generator = utils.generate_all_html_files_in_dir(source_dir, print_progress)

    lang_pipelines = {}

    for lang in common.LANGUAGES:
        with open('./res/task3/task3_model_{}_good.pickle'.format(lang), 'rb') as f:
            lang_pipelines[lang]= pickle.load(f)

    with Manager() as manager:

        # cat -> article_filename -> importance
        cat_articles = manager.dict({cat:manager.list() for cat in CATS})

        classify_fun = ClassifyClass(cat_articles, lang_pipelines, output_dir).classify

        with Pool(common.NUM_CORES) as p:
            p.map(classify_fun, html_files_generator)

        result = [
            {
                'category': k,
                'articles': sorted(v)
            }
            for k,v in cat_articles.items()
        ]

    if output_dir:
        json_file = os.path.join(output_dir, 'results.json')
        utils.print_to_json(result, json_file)
    if print_to_stdout:
        result_str = json.dumps(result, ensure_ascii=False, indent=3)
        print(result_str)

    return result

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--sites', action='store_true')
    parser.add_argument('--dataset', action='store_true')
    parser.add_argument('--lang') # used in --sites and --dataset
    parser.add_argument('--output_html_files', action='store_true') # used in --sites and --dataset
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--evaluate', action='store_true')
    parser.add_argument('--df') # file jsonl used in evalute
    args = parser.parse_args()

    if args.sites:        
        run_sites_analysis(args.lang, args.output_html_files)
    elif args.dataset:
        build_dataset(args.lang, args.output_html_files)
    elif args.train:
        Train().run()
    elif args.evaluate:
        Evaluate(args.df).run()