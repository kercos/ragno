import os
import re
import json
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import scipy
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.cluster import AgglomerativeClustering
from collections import defaultdict
from multiprocessing import Pool, Manager
import string
import seaborn as sns
from src import utils as utils
from src import task1
from src import common
from src.sort_by_importance import get_importance 
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.metrics import pairwise_distances
# import utils

'''
Launch parameters:

tgnews threads source_dir
The result must be output to STDOUT in JSON format.

Output format:

[
  {
    "title": "Telegram announced Data Clustering Contest",
    "articles": [
      "6354183719539252.html",
      ...
    ]
  },
  {
    "title": "Apple reveals new AirPods Pro",
    "articles": [
      "9436743547232134.html",
      ...
    ]
  },
  ...
]
where:

title – thread title
articles – list of file names containing articles in the thread, sorted by their relevances (most relevant at the top)
'''

WORD_RE = re.compile(r'\w+')

DISTANCE_THRESHOLD = 0.73

LANG_STOP_WORDS = {
    lang: json.load(open('./res/lang/stop_words_{}.json'.format(lang))) 
    for lang in common.LANGUAGES
}


def compute_distance(sw1, sw2):
    num_common_words = len(sw1.intersection(sw2))
    if not num_common_words:
        distance = -1
    else:
        num_total_words = len(sw1.union(sw2))
        distance = num_total_words/num_common_words
    return distance


def clean_text(text):    
    text = text.lower()
    text = text.replace('\n',' ')    
    t = str.maketrans(dict.fromkeys(string.punctuation, ' '))
    text = text.translate(t)
    return text

def prepare_html_files(html_files):

    basenames = []
    titles = []
    relevances = []
    tokens_texts = []
    meta_info_body_texts = []

    for f in html_files:
        file_basename = os.path.basename(f)
        meta_info, body_text = utils.parse_html(f)
        lang, _ = task1.infer_lang(meta_info, body_text)

        if lang is None:
            continue

        full_t = '\n'.join([
            meta_info['title'],
            meta_info['description'],
            body_text
        ])
        _, dom = utils.get_base_url_and_domain(meta_info)
        cleaned_t = clean_text(full_t)
        titles.append(meta_info['title'])
        relevances.append(get_importance(lang,dom))
        basenames.append(file_basename)
        tokens = set([t for t in WORD_RE.findall(cleaned_t) if t not in LANG_STOP_WORDS[lang]])
        tokens_texts.append(tokens)
        meta_info_txt = json.dumps(meta_info, ensure_ascii=False, indent=3)
        meta_info_body_texts.append('\n\n'.join([meta_info_txt, body_text]))
    
    return {
        'basenames': basenames, 
        'titles': titles, 
        'relevances': relevances, 
        'tokens_texts': tokens_texts,
        'meta_info_body_texts': meta_info_body_texts
    }

def prepare_articles(articles, lang):
    
    basenames = []
    titles = []
    relevances = []
    tokens_texts = []
    meta_info_body_texts = []

    for art in articles:
        file_basename = art.name
        meta_info, body_text = art.meta_info, art.body_text

        full_t = '\n'.join([
            meta_info['title'],
            meta_info['description'],
            body_text
        ])
        _, dom = utils.get_base_url_and_domain(meta_info)
        cleaned_t = clean_text(full_t)
        titles.append(meta_info['title'])
        relevances.append(get_importance(lang,dom))
        basenames.append(file_basename)
        tokens = set([t for t in WORD_RE.findall(cleaned_t) if t not in LANG_STOP_WORDS[lang]])
        tokens_texts.append(tokens)
        meta_info_txt = json.dumps(meta_info, ensure_ascii=False, indent=3)
        meta_info_body_texts.append('\n\n'.join([meta_info_txt, body_text]))
    
    return {
        'basenames': basenames, 
        'titles': titles, 
        'relevances': relevances, 
        'tokens_texts': tokens_texts,
        'meta_info_body_texts': meta_info_body_texts
    }

def run_clustering(processed_articles, output_dir, sort_threads=False, print_plots=False):

    basenames = processed_articles['basenames']
    titles = processed_articles['titles']
    relevances = processed_articles['relevances']
    tokens_texts = processed_articles['tokens_texts']
    meta_info_body_texts = processed_articles['meta_info_body_texts']

    vectorizer = HashingVectorizer(
        ngram_range=(1,6),
        analyzer='word',
        stop_words=None)

    if len(meta_info_body_texts)==0:
        return [], []

    hashed_texts = vectorizer.fit_transform(meta_info_body_texts)

    square_matrix = pairwise_distances(hashed_texts, Y=None, metric='cosine')
    condensed_distance_matrix = scipy.spatial.distance.squareform(square_matrix)

    if len(condensed_distance_matrix)==0:
        return [], []

    num_docs = len(basenames)
    assert num_docs == hashed_texts.shape[0]

    if False:
        condensed_distance_matrix = []
        for i in range(num_docs):
            for j in range(i+1, num_docs):
                condensed_distance_matrix.append(
                    compute_distance(tokens_texts[i], tokens_texts[j])
                )

    max_distance = max(condensed_distance_matrix)
    condensed_distance_matrix = [max_distance if d==-1 else d for d in condensed_distance_matrix]

    if print_plots:    
        sns.distplot(condensed_distance_matrix)
        print("Num docs: {}".format(num_docs))
        print("Size condensed_distance_matrix: {}".format(len(condensed_distance_matrix)))                
        print("Max distance: {}".format(max_distance))
        print("Min distance: {}".format(min(condensed_distance_matrix)))
    
    condensed_distance_matrix = np.asarray(condensed_distance_matrix, dtype=np.float32)
    
    linkage_type = 'complete' # “complete”, “average”, “single”}

    if print_plots:
        linked = linkage(condensed_distance_matrix, linkage_type)    
        labelList = range(1, num_docs+1)    
        plt.figure(figsize=(10, 7))
        dendrogram( linked,
                    orientation='top',
                    labels=labelList,
                    distance_sort='descending',
                    show_leaf_counts=True)
        plt.show()

    square_matrix = scipy.spatial.distance.squareform(condensed_distance_matrix)
    cluster = AgglomerativeClustering(
        n_clusters=None, 
        affinity='precomputed', 
        linkage=linkage_type, 
        distance_threshold=DISTANCE_THRESHOLD
    )

    clustering = cluster.fit_predict(square_matrix)
    
    # list of labels for each article in input list
    clusters_list = list(clustering) 
    
    thread_num_indexes = defaultdict(list)

    for i,label in enumerate(clusters_list):        
        thread_num_indexes[int(label)].append(i)

    # list of indexes, one per thread
    threads_list_indexes = list(thread_num_indexes.values())

    result = []
    threads_relevance = []

    for thread_index, list_indexes in enumerate(threads_list_indexes):
        group_titles = [titles[i] for i in list_indexes]
        group_basenames = [basenames[i] for i in list_indexes]
        group_relevance = [relevances[i] for i in list_indexes]
        min_title_length = min(len(t) for t in group_titles)
        shortest_titles = next(t for t in group_titles if len(t)==min_title_length)        
        basenames_relevance = dict(zip(group_basenames, group_relevance))
        sorted_articles = [
            kv[0] for kv in sorted(basenames_relevance.items(), 
            key=lambda kv: -kv[1]) 
        ]
        result.append(
            {
                "title": shortest_titles,
                "articles": sorted_articles
            }            
        )
        if sort_threads:
            current_relevance = sum(group_relevance) / len(group_relevance)
            threads_relevance.append(current_relevance)            
        if output_dir:
            for file_basename in sorted_articles:
                file_name = file_basename.replace('.html', '.txt')
                output_file_path = os.path.join(output_dir, str(thread_index), file_name)
                parent_dir = os.path.dirname(output_file_path)
                if not os.path.exists(parent_dir):
                    os.makedirs(parent_dir)
                with open(output_file_path, 'w') as f_out:
                    article_index = basenames.index(file_basename)
                    f_out.write(meta_info_body_texts[article_index])            

    return result, threads_relevance

def run_threads_task(source_dir, output_dir=None, print_to_stdout=True, print_plots=False):

    # do not print progress bar if printing result to stdout    
    print_progress = not print_to_stdout 

    html_generator = utils.generate_all_html_files_in_dir(source_dir, print_progress)

    processed_articles = prepare_html_files(html_generator)
    result, _ = run_clustering(processed_articles, output_dir, sort_threads=False, print_plots=print_plots)            

    if output_dir:
        json_file = os.path.join(output_dir, 'results.json')
        utils.print_to_json(result, json_file)
    if print_to_stdout:
        result_str = json.dumps(result, ensure_ascii=False, indent=3)
        print(result_str)



if __name__ == "__main__":
    source_dir = './here_en_cats/html/technology'
    output_dir = './test_new/'
    run_threads_task(source_dir, output_dir, print_plots=True)
