import re
import sys
import argparse
import time
import json
import random
import pandas as pd
import numpy as np
import gensim
from gensim.models.callbacks import CallbackAny2Vec
import smart_open
from matplotlib import pyplot as plt
import scipy
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import FeatureUnion
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import pairwise_distances
from collections import defaultdict
from sklearn.metrics import silhouette_score
import string
import seaborn as sns
from src import common

word_re = re.compile(r'\w+')

def compute_distance(sw1, sw2):
    num_common_words = len(sw1.intersection(sw2))
    if not num_common_words:
        distance = -1
    else:
        num_total_words = len(sw1.union(sw2))
        distance = num_total_words/num_common_words
    return distance

def compute_distance_doc2vec(sw1_vector, sw2_vector):
    distance = scipy.spatial.distance.cosine(sw1_vector, sw2_vector)
    return distance


def clean_text(text):
    text = text.lower()
    text = text.replace('\n',' ')
    t = str.maketrans(dict.fromkeys(string.punctuation, ' '))
    text = text.translate(t)
    return text

def cluster_sentences(json_file_path, distance_threshold, print_plots, doc2vec:bool=False, remove_stop_words:bool=True, use_hashing:bool=False):
    df = pd.read_json(json_file_path, orient='records', lines=True)
    docs_bow = []
    for_hashing_texts = []
    titles = df['title'].tolist()
    descriptions = df['description'].tolist()
    body_texts = df['body_text'].tolist()
    stop_words_file = './res/stop_words_en.json'

    if doc2vec:
        doc2vec_model = gensim.models.doc2vec.Doc2Vec.load('./res/task4_embeddings_dataset/doc2vec_en.pkl')

    with open(stop_words_file) as f_in:
        stop_words = set(json.load(f_in))

    num_docs = len(titles)

    for n, tdb in enumerate(zip(titles, descriptions, body_texts),1):
        full_text = '\n'.join(tdb)
        full_text = clean_text(full_text)
        if use_hashing:
            for_hashing_texts.append(full_text)
        elif remove_stop_words:
            tokens = [t for t in word_re.findall(full_text) if t not in stop_words]
            doc_tokens_set = set(tokens)
            docs_bow.append(doc_tokens_set)
        else:
            docs_bow.append(full_text.split(' '))

    print("Num docs: {}".format(num_docs))

    if use_hashing:
        #vectorizer = HashingVectorizer(
        vectorizer = TfidfVectorizer(
            ngram_range=(1,2),
            analyzer='word',
            stop_words=list(stop_words) if remove_stop_words else None)
        pipeline_vectorizer = FeatureUnion([
            ('wostop', HashingVectorizer(ngram_range=(1,1), stop_words=stop_words)),
            ('wstop', HashingVectorizer(ngram_range=(3,5), stop_words=None))],
                                           transformer_weights={'wostop': 1, 'wstop':2})
        hashed_texts = vectorizer.fit_transform(for_hashing_texts)
        assert num_docs == hashed_texts.shape[0]

    if doc2vec:
        vectorized_docs = [doc2vec_model.infer_vector(doc) for doc in docs_bow]
        assert len(vectorized_docs) == num_docs

    if use_hashing:
        square_matrix = pairwise_distances(hashed_texts, Y=None, metric='cosine')
        condensed_distance_matrix = scipy.spatial.distance.squareform(square_matrix)
        max_distance = max(condensed_distance_matrix)
    else:
        condensed_distance_matrix = []
        for i in range(num_docs):
            for j in range(i+1, num_docs):
                if doc2vec:
                    distance = compute_distance_doc2vec(vectorized_docs[i], vectorized_docs[j])
                else:
                    distance = compute_distance(docs_bow[i], docs_bow[j])
                condensed_distance_matrix.append(distance)
        print("Size condensed_distance_matrix: {}".format(len(condensed_distance_matrix)))
        max_distance = max(condensed_distance_matrix)
        condensed_distance_matrix = [max_distance if d==-1 else d for d in condensed_distance_matrix]
 

    if print_plots:    
        sns.distplot(condensed_distance_matrix);

    print("Max distance: {}".format(max_distance))
    print("Min distance: {}".format(min(condensed_distance_matrix)))
    
    condensed_distance_matrix = np.asarray(condensed_distance_matrix, dtype=np.float32)
    
    linkage_type = 'complete' # “complete”, “average”, “single”}

    if print_plots:
        linked = linkage(condensed_distance_matrix, linkage_type)    
        labelList = range(1, num_docs+1)    
        plt.figure(figsize=(10, 7))
        dendrogram( linked,
                    orientation='top',
                    labels=labelList,
                    distance_sort='descending',
                    show_leaf_counts=True)
        plt.show()

    square_matrix = scipy.spatial.distance.squareform(condensed_distance_matrix)
    cluster = AgglomerativeClustering(
        n_clusters=None, 
        affinity='precomputed', 
        linkage=linkage_type, 
        distance_threshold=distance_threshold
    )
    clustering = cluster.fit_predict(square_matrix)
    clusters_list = list(clustering)
    silhouette = silhouette_score(square_matrix, clusters_list)
    print('***********')
    print(f'Silhouette score: {silhouette}')
    print('***********')
    sent_groups = defaultdict(list)
    for i,label in enumerate(clusters_list):
        sent_groups[int(label)].append(titles[i])
    print(json.dumps(sent_groups, indent=3, ensure_ascii=False))


class EpochLogger(CallbackAny2Vec):
    '''Callback to log information about training'''

    def __init__(self):
        self.epoch = 0

    def on_epoch_begin(self, model):
        print("Epoch #{} start".format(self.epoch))

    def on_epoch_end(self, model):
        print("Epoch #{} end".format(self.epoch))
        self.epoch += 1



class Train:
    def __init__(self):
        self.train= './res/task4_embeddings_dataset/ru_train.uniq.txt'
        return None

    @staticmethod
    def read_corpus(fname, tokens_only=False):
        with smart_open.open(fname, encoding="iso-8859-1") as f:
            for i, line in enumerate(f):
                tokens = gensim.utils.simple_preprocess(line)
                if tokens_only:
                    yield tokens
                else:
                    # For training data, add tags
                    yield gensim.models.doc2vec.TaggedDocument(tokens, [i])


    def run(self, evaluate:bool=False):
        train_corpus = list(Train.read_corpus(self.train))

        epoch_logger = EpochLogger()

        model = gensim.models.doc2vec.Doc2Vec(vector_size=42, min_count=2, epochs=40, callbacks=[epoch_logger])
        print('Model defined...')

        model.build_vocab(train_corpus)
        print('Vocab created...')

        print('Starting training now...')
        model.train(train_corpus, total_examples=model.corpus_count, epochs=model.epochs)
        print('Training done!')

        if evaluate:
            # Pick a random document from the test corpus and infer a vector from the model
            test_corpus = list(Train.read_corpus(self.test, tokens_only=True))
            doc_id = random.randint(0, len(test_corpus) - 1)
            inferred_vector = model.infer_vector(test_corpus[doc_id])
            sims = model.docvecs.most_similar([inferred_vector], topn=len(model.docvecs))

            # Compare and print the most/median/least similar documents from the train corpus
            print('Test Document ({}): «{}»\n'.format(doc_id, ' '.join(test_corpus[doc_id])))
            print(u'SIMILAR/DISSIMILAR DOCS PER MODEL %s:\n' % model)
            for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
                print(u'%s %s: «%s»\n' % (label, sims[index], ' '.join(train_corpus[sims[index][0]].words)))

        with open('./res/task4_embeddings_dataset/doc2vec_ru.pkl', 'wb+') as f:
            model.save(f)

        return None



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-embeddings', action='store_true')
    parser.add_argument('--use-doc2vec', action='store_true')
    parser.add_argument('--use-hashing', action='store_true')
    args = parser.parse_args()

    if args.train_embeddings:
        Train().run()
    elif args.use_doc2vec:
        json_file_path = './res/task4_en_science_all.jsonl'
        cluster_sentences(json_file_path, distance_threshold=0.5, print_plots=True, doc2vec=True, remove_stop_words=False)
    elif args.use_hashing:
        json_file_path = './res/task4_en_science_all.jsonl'
        cluster_sentences(json_file_path, distance_threshold=0.95, print_plots=True, doc2vec=False, remove_stop_words=True, use_hashing=True)
    else:
        json_file_path = './res/task4_en_science_all.jsonl'
        cluster_sentences(json_file_path, distance_threshold=10, print_plots=True)
