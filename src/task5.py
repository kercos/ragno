import os
import pickle
import json
from multiprocessing import Pool, Manager
from src.utils import generate_all_html_files_in_dir
from src import common
from src import utils
from src import task3
from src import task4

'''
Launch parameters:

tgnews top source_dir
The result must be output to STDOUT in JSON format.

Output format:

[
  {
    "category": "any",
    "threads": [
      {
        "title": "Telegram announces Data Clustering Contest",
        "category": "technology",
        "articles": [
          "6354183719539252.html",
          ...
        ]
      },
      {
        "title": "Apple reveals new AirPods Pro",
        "category": "technology",
        "articles": [
          "9436743547232134.html",
          ...
        ]
      },
      ...
    ]
  },
  {
    "category": "technology",
    "threads": [
      {
        "title": "Telegram announces Data Clustering Contest",
        "articles": [
          "6354183719539252.html",
          ...
        ]
      },
      ...
    ]
  },
  {
    "category": "sports",
    "threads": [
      ...
    ]
  },
  ...
]
where:

category – "society", "economy", "technology", "sports", "entertainment", "science" or "other". You must also include a block with category="any", containing a list of threads, indepedent of category, sorted by relevance (most relevant at the top).
threads – list of threads, sorted by relevance (most relevant at the top). Each thread contains a title and list of articles. If category="any", also includes category.
title – thread title.
articles – list of names of files containing articles from the thread, sorted by relevance (most relevant at the top).
'''


def sort_threads_by_relevance(threads_structure, threads_relevance):

    # thread_index -> avg relevance
    index_relevance = {
        i: threads_relevance[i]
        for i in range(len(threads_relevance))
    }

    sorted_indexes = [
        i for i, rel in sorted(
            index_relevance.items(),
            key=lambda i_rel: -i_rel[1]
        )
    ]

    return [threads_structure[i] for i in sorted_indexes]


def run_top_task(source_dir, output_dir=None, print_to_stdout=True):

    # do not print progress bar if printing result to stdout
    print_progress = not print_to_stdout

    # generator of html files in source dir
    html_files_generator = utils.generate_all_html_files_in_dir(
        source_dir, print_progress)

    file_base_path_map = {os.path.basename(f): f for f in html_files_generator}

    task3_result = task3.run_categories_task(
        source_dir, output_dir=None, print_to_stdout=False, print_progress=False)

    result = []

    any_threads = []
    any_threads_relevance = []

    for cat_item in task3_result:

        category = cat_item['category']
        cat_articles = cat_item['articles']

        if not cat_articles:

            cat_threads_sorted = []

        else:

            cat_files = [v for k, v in file_base_path_map.items()
                         if k in cat_articles]

            cat_threads, threads_relevance = task4.run_clustering(cat_files,
                                                                  output_dir=None, sort_threads=True, print_plots=False)

            cat_threads_sorted = sort_threads_by_relevance(
                cat_threads, threads_relevance)

            for thread_item, relevance in zip(cat_threads, threads_relevance):
                thread_title = thread_item['title']
                thread_articles = thread_item['articles']
                any_threads.append(
                    {
                        "title": thread_title,
                        "category": category,
                        "articles": thread_articles
                    }
                )
                any_threads_relevance.append(relevance)

        result.append(
            {
                "category": category,
                "threads": cat_threads_sorted
            }
        )

    any_threads_sorted = sort_threads_by_relevance(
        any_threads, any_threads_relevance)
    result.insert(
        0,
        {
            "category": "any",
            "threads": any_threads_sorted
        }
    )

    if output_dir:
        json_file = os.path.join(output_dir, 'results.json')
        utils.print_to_json(result, json_file)
    if print_to_stdout:
        result_str = json.dumps(result, ensure_ascii=False, indent=3)
        print(result_str)


if __name__ == '__main__':
    print(sort_threads_by_relevance(['a', 'b', 'c'], [1, 2, 3]))
