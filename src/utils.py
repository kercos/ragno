import sys
import os
import re
from bs4 import BeautifulSoup
from shutil import copyfile
import json

def get_total_html_files_in_dir(src_dir_path):
    total_files = sum(
        [len([f for f in files if f.endswith('.html')]) 
        for r, d, files in os.walk(src_dir_path)]
    )   
    return total_files 

def generate_all_html_files_in_dir(src_dir_path, print_progress=False):
    
    if print_progress:
        total_files = get_total_html_files_in_dir(src_dir_path)
        print("Total html files: {}".format(total_files))
    num = 0
    for root, directories, filenames in os.walk(src_dir_path):
        for filename in filenames: 
            if filename.endswith('.html'):
                num += 1
                if print_progress:
                    print_progress_bar(num, total_files)
                yield os.path.join(root,filename)

def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=50, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    sys.stdout.write('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix))
    sys.stdout.flush()
    # Print New Line on Complete
    if iteration == total:
        print()

# HTML_TAG_RE = re.compile(r'<.+?>')
# HEADER_TAG_RE = re.compile(r'^<h\d>')
# SPACE_RE = re.compile(r'[ ]+')
# PUNCT_RE = re.compile(r'([;.,:!?…()‹›«»"”“\'’`‘\-])')

BODY_TAGS_RE = re.compile('^(?:h[1-9]|p)$') #<h> tags or <p> tags

def get_tag_property_content(soup, tag, prop):
    field = soup.find(tag,  property=prop)
    return field['content'] if field else ''

def get_tag_property_text(soup, tag, **argv):
    field = soup.find(tag,  argv)
    return field.get_text() if field else ''

def parse_html(html, is_file = True):
    
    bs_input = open(html) if is_file else html
    soup = BeautifulSoup(bs_input, 'html.parser')

    url = get_tag_property_content(soup, 'meta', "og:url")    
    site_name = get_tag_property_content(soup, 'meta', "og:site_name")
    title = get_tag_property_content(soup, 'meta', "og:title")
    date_time = get_tag_property_content(soup, 'meta', "article:published_time")
    description = get_tag_property_content(soup, 'meta', "og:description")    
    author = get_tag_property_text(soup, 'a', rel="author")

    # get body
    body_fields = soup.find('body').find_all(BODY_TAGS_RE)
    body_lines = [f.get_text().strip() for f in body_fields]
    body_text = '\n'.join([l for l in body_lines if l])

    meta_info = {
        'url': url,
        'site_name': site_name,
        'title': title,
        'date_time': date_time,
        'description': description,
        'author': author
    }

    # return html elements
    return meta_info, body_text

def get_base_url_and_domain(meta_info):
    url = meta_info['url']
    if url.endswith('/'):
        url = url[:-1]
    base_url = url.rsplit('/',1)[0]
    domain = url.split('/')[2]
    return base_url.lower(), domain.lower()

def print_to_json(struct, output_file):
    with open(output_file, 'w') as f_out:
        json.dump(struct, f_out, ensure_ascii=False, indent=3)            

def print_sorted_collection_to_file(collection, output_file):
    sorted_collection = sorted(collection)
    with open(output_file, 'w') as f_out:
        for e in sorted_collection:
            f_out.write('{}\n'.format(e))            

def print_dictint_to_file_sorted(output_fie, dictint):
    with open(output_fie, 'w') as f_out:
        for d,f in sorted(dictint.items(), key=lambda df: -df[1]):
            f_out.write("{}\t{}\n".format(f,d))


if __name__ == "__main__":
    pass